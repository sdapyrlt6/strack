from django.shortcuts import render, reverse
from django.views.generic import TemplateView, CreateView
from strack.forms import CustomUserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin


class LandingPageView(LoginRequiredMixin, TemplateView):
    template_name = 'landingpage.html'


class SignupView(CreateView):
    template_name = 'registration/signup.html'
    form_class = CustomUserCreationForm

    def get_success_url(self):
        return reverse('login')
